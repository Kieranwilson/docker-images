# My Dev Build with X11

This is the docker image I use for local development with xDebug and PhpStorm

## Macintosh

```
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\" & SOCAT_PID=$!
docker run -e DISPLAY=$(ifconfig vboxnet0 | grep "inet" | cut -d " " -f 2):0 -it devlocal bash
kill $SOCAT_PID
```

### Prerequisites

Requires Docker and xQuartz

## Windows

```
runDocker()
{
	command="docker run -p 9000:9000 --name dev-machine -e DISPLAY=10.0.75.1:0.0 --rm -v C:/WebProjects/dockerized-dev/home:/home -v C:/WebProjects/dockerized-dev/root:/root -v /root/.ssh"
	commandEnd="-it dev bash"

	for var in "$@"
	do
		fileName=`echo $var | sed 's/.*\///'`
		command="$command -v $var:/home/$fileName"
	done

	echo "executing: $command $commandEnd"
	eval "$command $commandEnd"
}

alias devrun="runDocker C:/WebProjects"
alias devbuild="docker build -f C:/WebProjects/dockerized-dev/dev-phpstorm-xdebug/Dockerfile -t dev C:/WebProjects/dockerized-dev/dev-phpstorm-xdebug/"
alias devconnect="docker exec -it dev-machine bash"
```

### Prerequisites

Requires Docker and xMing
